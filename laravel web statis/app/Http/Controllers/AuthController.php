<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function auth()
    {
        return view ('register');
    }

    public function kirim(Request $request)
    {
        $namaDepan = $request['namadepan'];
        $namaBelakang = $request['namabelakang'];

        return view('welcome', ["namaDepan" => $namaDepan , "NamaBelakang" => $namaBelakang]);
    }
    
}
