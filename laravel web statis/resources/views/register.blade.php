<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <form action="/kirim" method="POST">
        @csrf
        <h1>Buat Account Baru!</h1>
        <p><b>Sign Up Form</b></p>
        <label>First name:</label>
        <br><br>
        <input type="text" name="namadepan">
        <br><br>
        <label>Last name:</label>
        <br><br>
        <input type="text" name="namabelakang">
        <br><br>
        <p>Gender:</p>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br>
        <label>Nationalitiy:</label>
        <br><br>
        <select name="Nationalitiy" id="Nationalitiy">
            <option value="volvo">Indonesian</option>
            <option value="saab">Korea</option>
            <option value="mercedes">Jepang</option>
            <option value="audi">Malaysia</option>
            <option value="volvo">Singapura</option>
            <option value="saab">Thailand</option>
            <option value="mercedes">Australia</option>
            <option value="audi">India</option>
        </select>
        <br><br>
        <label for="language">Language Spoken:</label>
        <br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br>
        <br>
        <label for="bio">Bio:</label>
        <br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" name="submit" value="Sign Up"/>
    </form>
</body>
</html>